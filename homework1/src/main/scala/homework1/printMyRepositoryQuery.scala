//Jamal Shoubaki
//Query 2: Print my Repository
/*Description: What this query will do will print the last amount of repos the Bearer has added.
  The user will be prompted on the terminal how many they would wish to see. Simply type in a number and
  Watch the repo show via Nodes

  Unit Test 2: (showNodes) will check to make sure all the correct Nodes are visible
  and correct. The Test is if the numRepos = 1 and it is my token, if the name is Escape Room Prototype
  then the test is successful
 */

package homework1

import java.io.{File, PrintWriter}
import java.util.Calendar

import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import org.json4s.DefaultFormats
import org.json4s.native.JsonParser.parse

import scala.io.Source

object printMyRepositoryQuery {

  def main(args: Array[String]): Unit = {
    val writer= new PrintWriter(new File("Query2.log"))

    //Access API Endpoint (In this case Github GraphQL)
    val BASE_GHQL_URL = "https://api.github.com/graphql"
    implicit val formats = DefaultFormats

    val client = HttpClientBuilder.create().build()
    val httpUriRequest = new HttpPost(BASE_GHQL_URL)
    val Bearer ="Bearer 17bec296a0a2e71406aaab6379b46c1760e1cfdd"

    print("Query 2: Latest Repositories \n\nDescription: What this query will do will print the last amount of repos the Bearer has added.\n  The user will be prompted on the terminal how many they would wish to see. Simply type in a number and\n  Watch the repo show via Nodes" +
      "\n\nEnter the number of your most recent Repositories you Want to see: ")
    writer.write(Calendar.getInstance().getTime() + "Query 2 has been opened")
    val numRepos: Int= scala.io.StdIn.readInt()
    print("\n")
    httpUriRequest.addHeader("Authorization", Bearer)
    httpUriRequest.addHeader("Accept", "application/json")
    val gqlReq = new StringEntity("{\"query\":\"" +"query{viewer {login repositories(last:"+ numRepos+"){nodes{name}}}}\"}")

    httpUriRequest.setEntity(gqlReq)

    val response1 = client.execute(httpUriRequest)
    System.out.println("Response:" + response1)

    response1.getEntity match {
      case null => System.out.println("Response entity is null")
      case x if x != null => {
        val respJson = Source.fromInputStream(x.getContent).getLines.mkString
        //System.out.println(respJson)
        val viewer = parse(respJson).extract[RootInterface]
        System.out.println("\nUsername: " +viewer.data.viewer.login
          +"\nRepositories: \n" )

        writer.write("\n" +Calendar.getInstance().getTime() + "Username Found: " +viewer.data.viewer.login )

        def printList(args: TraversableOnce[_]): Unit = {
          args.foreach(println)
        }

        printList(viewer.data.viewer.repositories.nodes)



        //Test 2:Will check to see if my head node is Nodes(EscapeRoomPrototype)
        // when number of repositories is 1 and using my token
        def Test2_showNodes(): Unit = {
          if(Bearer.compareTo("Bearer 17bec296a0a2e71406aaab6379b46c1760e1cfdd")==0) {

            if (numRepos.equals(1)) {
              var test2: String = "Nodes(EscapeRoomPrototype)"
              if (test2.compareTo(viewer.data.viewer.repositories.nodes.head.toString()) == 0) {

                println("\n\nTest 2 has been passed")
                writer.write("\n" + Calendar.getInstance().getTime() + "Test 2 has Passed")
              }
            }
          }
        }

          Test2_showNodes()



          writer.close()


      }
    }
  }
}
