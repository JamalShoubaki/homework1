//Jamal Shoubaki
//Query 1: Viewer Basic Info
/*Description: What this query will do is provide the users basic information (name, url, login, bio)
  This builds upon the sample program provided with a more descriptive query of the viewer

  Test 1 (showSameName): Check the name of the user based on their token (in this case my token)
  and will print either -1, 0, or 1. If the answer is "0" then the name matches
 */
package homework1

//Imported Libraries
import java.io.{File, PrintWriter}
import java.util.Calendar

import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import org.json4s.DefaultFormats
import org.json4s.native.JsonParser.parse

import scala.io.Source

object viewerInfoQuery{

  def main(args: Array[String]): Unit = {
    val writer= new PrintWriter(new File("Query1.log"))


    println("Query 1: Viewer Basic Info\n\n" +
      "Description: What this query will do is provide the users basic information (name, url, login, bio)\n  " +
      "This builds upon the sample program provided with a more descriptive query of the viewer")
      writer.write(Calendar.getInstance().getTime() + "Query 1 has been opened\n")

    //Access API Endpoint (In this case Github GraphQL)
    val BASE_GHQL_URL = "https://api.github.com/graphql"
    implicit val formats = DefaultFormats

    val client = HttpClientBuilder.create().build()
    val httpUriRequest = new HttpPost(BASE_GHQL_URL)
    val Bearer ="Bearer 17bec296a0a2e71406aaab6379b46c1760e1cfdd"
    httpUriRequest.addHeader("Authorization", Bearer)
    httpUriRequest.addHeader("Accept", "application/json")
    val gqlReq2 = new StringEntity("{\"query\":\"" +"{viewer {name url login email bio}}\"}")

    httpUriRequest.setEntity(gqlReq2)

    val response1 = client.execute(httpUriRequest)
    System.out.println("Response:" + response1)

    response1.getEntity match {
      case null => System.out.println("Response entity is null")
      case x if x != null => {
        val respJson = Source.fromInputStream(x.getContent).getLines.mkString
        //System.out.println(respJson)
        val viewer = parse(respJson).extract[RootInterface]
        System.out.println("\nName: " + viewer.data.viewer.name + "\n"
          + "URL: " + viewer.data.viewer.url + "\n"
          + "Username: " +viewer.data.viewer.login
          +"\nBio: " + viewer.data.viewer.bio)
        writer.write("\n"+Calendar.getInstance().getTime() +"Name requested: " + viewer.data.viewer.name)
       //Test 1: Will check to see if the name is the same with my token
        def Test1_showSameName(): Unit ={

          val test2=   "Jamal Shoubaki"
          if(Bearer.compareTo("Bearer 17bec296a0a2e71406aaab6379b46c1760e1cfdd")==0) {
            if (test2.compareTo(viewer.data.viewer.name) == 0) {

              println("\n\nTest 1 has been passed")
              writer.write("\n"+ Calendar.getInstance().getTime() + "Test 1 has been passed\n")
            }
          }
        }




        Test1_showSameName()
        writer.close()
      }
    }
  }
}
